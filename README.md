# Clocktimer

## Synopsis

Clocktimer is a countdown timer made with Python 3 and PyQt5 with custom presets

![alt text](https://3dminfographie.com/images/scripts/clocktimer/example.png "clocktimer")

## Installation

### Archlinux :

Install the dependencies **python3**, **python-pyqt5** and **qt5-multimedia**

``` # pacman -S python3 python-pyqt5 qt5-multimedia ```

### Ubuntu / debian : 

Install the dependencies **python3**, **python3-pyqt5**, **python3-pyqt5.qtmultimedia** and **libqt5multimedia5-plugins**

``` # apt install python3 python3-pyqt5 python3-pyqt5.qtmultimedia libqt5multimedia5-plugins ```

### After dependencies :

Download clocktimer.zip and Extract

## Experiments examples - demo files

You can changed some settings on preferences panel :

(Preferences are saved in your user file **~/.config/clocktimer** )

![alt text](https://3dminfographie.com/images/scripts/clocktimer/example_preferences.png "clocktimer")

There is 2 Themes (default or dark)

Here the dark one

![alt text](https://3dminfographie.com/images/scripts/clocktimer/example_dark.png "clocktimer")

## Tests

``` $ cd clocktimer-master ```

``` $ python3 clocktimer.py ```

Or

Add a launcher in your desktop environment

### Attention :

There is actually 2 Bugs with custom sound and dark theme. After Save your preferneces, You need to quit the program to have it.

I'm sorry for that !

## License

GNU General Public License v3.0 only - GPL-3.0 - Damien Monteillard - 2020
