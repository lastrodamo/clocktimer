#!/usr/bin/env python3

#################################################################
#------------------------ clocktimer ---------------------------#
#            Python3/Python-PyQt5/Qt5-multimedia                #
#                                                               #
#                                                               #
#------------------------ version 1.0 --------------------------#
#                                                               #
#------------- by Damien Monteillard 19/01/2020 ----------------#
#                                                               #
#                                                               #
#                      Damien Monteillard                       #
#                        Sylvain Philip                         #
#                       Antonio Calculon                        #
#                            (2020)                             #
#                                                               #
#------------ GNU General Public License v3.0 only -------------#
#------------------------- GPL-3.0 -----------------------------#
#                                                               #
#                      Sound by maphill                         #
#      https://freesound.org/people/maphill/sounds/204103/      #
#                                                               #
#################################################################

from PyQt5 import Qt, QtCore, QtWidgets, QtMultimedia, QtGui
from PyQt5.QtMultimedia import QMediaPlayer, QMediaContent, QMediaPlaylist
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QAction, QDialog, QLabel, QLineEdit, QPushButton, QVBoxLayout, QMessageBox, QSpinBox, QFileDialog, QFrame
from PyQt5.QtCore import *
from PyQt5.QtGui import *
import sys, os, json, hashlib

dir_path = os.path.dirname(os.path.realpath(__file__))

from dialog import Preferences

class Application(QApplication):
	def __init__(self, argv=None):
		super(Application, self).__init__(argv)

		conf_dir = QStandardPaths.locate(QStandardPaths.ConfigLocation, "", QStandardPaths.LocateDirectory)
		self.app_dir = QtCore.QFileInfo(conf_dir + "/clocktimer").absoluteFilePath()

		self.win = Window(self)
		self.win.show()

	def load_config(self):
		conf_file = QtCore.QFileInfo(self.app_dir + "/config.json").absoluteFilePath()

		config = {
			'dark' : 0,
			'tictac' : 2,
			'minutes' : 10,
			'secondes' : 0,
			'sid' : 0,
			'soundfile' : "",
			'volume' : 99,
			'period' : 10,
		}

		if QFile.exists(conf_file):
			h = open(conf_file, 'r')
			config = json.load(fp=h)

		return config

	def save_config(self, config):
		confDir = QDir(self.app_dir)

		if not confDir.exists():
			confDir.mkdir(self.app_dir)

		try:
			conf_file = open(self.app_dir + '/config.json', 'w')
			json.dump(config, fp=conf_file, indent=2, separators=(',', ':'))

		except SomeError as err:
			print('Error saving config: %s' % (err))

		conf_file.close()

	def loadDatabase(self):
		presetFile = QtCore.QFileInfo(self.app_dir + "/presets.json").absoluteFilePath()
		data = []

		if not os.path.isfile(presetFile):
			return data

		try:
			db = open(presetFile, 'r')
			data = json.load(fp=db)
		except SomeError as err:
			print('Error reading database: %s' % (err))

		db.close()

		data.sort(key=lambda k: k['name'], reverse=False)

		return data

	def saveDatabase(self, data):
		confDir = QDir(self.app_dir)

		if not confDir.exists():
			confDir.mkdir(self.app_dir)

		try:
			fileP = open(self.app_dir + '/presets.json', 'w')
			json.dump(data, fp=fileP, indent=2, separators=(',', ':'))

		except SomeError as err:
			print('Error saving database: %s' % (err))

		fileP.close()

	def savePreset(self, name, minutes, seconds):
		if (minutes == 0 and seconds == 0):
			return

		database = self.loadDatabase()
		hash_id = self.hashCreate(name, minutes, seconds)

		# Avoid doublon
		for i in range(len(database)):
			if database[i]['id'] == hash_id:
				return

		database.append({
			'id': hash_id,
			'name' : name,
			'minutes' : minutes,
			'seconds' : seconds,
		})

		self.saveDatabase(database)

	def hashCreate(self, name, minutes, seconds):
		hash_id = "%s%d%d" % (name, minutes, seconds)
		return hashlib.md5(hash_id.encode()).hexdigest()

	def deletePreset(self, hash_id):
		database = self.loadDatabase()

		for i in range(len(database)):
			if database[i]['id'] == hash_id:
				del database[i]
				break

		self.saveDatabase(database)

class Window(QMainWindow):
	def __init__(self, app=None):
		super(Window, self).__init__(None)

		self.app = app
		self.config = self.app.load_config()

		self.timer = QTimer(self)
		self.limit = QTimer(self)
		self.tictimer = QTimer(self)

		# Time in seconds set by user
		self.time = 0

		self.dark = self.config['dark']
		self.tictac = self.config['tictac']
		self.minutes = self.config['minutes']
		self.secondes = self.config['secondes']
		self.sid = self.config['sid']
		self.soundfile = self.config['soundfile']
		self.volume = self.config['volume']
		self.period = self.config['period']

		# QMediaPlayer
		self.player = QtMultimedia.QMediaPlayer()

		# Local directory and files
		ringoggpath = str(dir_path + '/sound/ringtimer.ogg')
		tictacoggpath = str(dir_path + '/sound/tic_tac.ogg')
		customabsolutefile = str(self.soundfile)

		if self.sid == 0:
			self.sound = QtMultimedia.QMediaContent(QtCore.QUrl.fromLocalFile(ringoggpath))
		else:
			self.sound = QtMultimedia.QMediaContent(QtCore.QUrl.fromLocalFile(customabsolutefile))

		self.soundtictac = QtMultimedia.QMediaContent(QtCore.QUrl.fromLocalFile(tictacoggpath))
		#soundtictac = QtMultimedia.QMediaContent(QtCore.QUrl.fromLocalFile(QtCore.QFileInfo("sound/tic_tac.ogg").absoluteFilePath()))
		self.player.setVolume(self.volume)

		# Main Menu
		mainMenu = self.menuBar()

		# QSpinBox Dark
		self.darkspin = QSpinBox()
		self.darkspin.setValue(self.dark)

		# Dark theme menu
		if self.dark == 2:
			mainMenu.setStyleSheet('background-color: #555; color: white')
		else:
			mainMenu.setStyleSheet('background-color: #eee; color: black')

		fileMenu = mainMenu.addMenu('File')
		editMenu = mainMenu.addMenu('Edit')
		helpMenu = mainMenu.addMenu('Help')

		exitpath = str(dir_path + '/images/exit.png')
		exitButton = QAction(QIcon(exitpath), 'Exit', self)
		exitButton.setShortcut("Ctrl+Q")
		exitButton.setStatusTip("Exit")
		exitButton.triggered.connect(self.exit)

		prefpath = str(dir_path + '/images/preferences.png')
		prefButton = QAction(QIcon(prefpath), 'Preferences', self)
		prefButton.setShortcut("Ctrl+P")
		prefButton.triggered.connect(self.pref)

		aboutpath = str(dir_path + '/images/about.png')
		helpButton = QAction(QIcon(aboutpath), 'About', self)
		helpButton.setShortcut("Ctrl+H")
		helpButton.setStatusTip("About")
		helpButton.triggered.connect(self.about)

		fileMenu.addAction(exitButton)
		editMenu.addAction(prefButton)
		helpMenu.addAction(helpButton)

		# Assign a Vbox layout to self
		vbox = QtWidgets.QVBoxLayout()

		hbox = QtWidgets.QHBoxLayout()
		vbox.addLayout(hbox)

		# Input QLabel
		self.lbl = QLabel("Please enter a remaining time")
		self.lbl.setAlignment(Qt.AlignCenter)
		vbox.addWidget(self.lbl)

		# Inputs
		hbox = QtWidgets.QHBoxLayout()
		vbox.addLayout(hbox)

		# Label Minutes
		self.lblmin = QLabel("Minutes")
		self.lblmin.setAlignment(Qt.AlignCenter)

		# Label Secondes
		self.lblsec = QLabel("Seconds")
		self.lblsec.setAlignment(Qt.AlignCenter)

		# Input QLineEdit minutes
		self.editmin = QSpinBox()
		self.editmin.setValue(self.minutes)
		self.editmin.setMinimum(0)
		self.editmin.setMaximum(99)
		self.editmin.setAlignment(Qt.AlignCenter)
		self.editmin.setFont(QFont('fonts/alarm_clock.ttf', 18, 50))

		# Input QLineEdit secondes
		self.editsec = QSpinBox()
		self.editsec.setValue(self.secondes)
		self.editsec.setMinimum(0)
		self.editsec.setMaximum(59)
		self.editsec.setAlignment(Qt.AlignCenter)
		self.editsec.setFont(QFont('fonts/alarm_clock.ttf', 18, 50))

		# Layout minutes seconds
		hbox.addWidget(self.lblmin)
		hbox.addWidget(self.editmin)
		hbox.addWidget(self.editsec)
		hbox.addWidget(self.lblsec)

		# QPushButton Start
		hbox = QtWidgets.QHBoxLayout()
		vbox.addLayout(hbox)

		self.startbutton = QPushButton()
		self.startbutton.setText("Start")
		self.startbutton.setStyleSheet('background-color: #37abc8; color: #111')
		#self.startbutton.setCheckable(True)
		#self.startbutton.toggle()
		self.startbutton.setEnabled(True)
		self.startbutton.clicked.connect(self.startTimer)

		# QPushButton Stop / Reset
		self.stopbutton = QPushButton()
		self.stopbutton.setText("Stop/Reset")
		self.stopbutton.setStyleSheet('background-color: #ff7f2a; color: #111')
		self.stopbutton.clicked.connect(self.stopTimer)

		# QSpinBox Tic Tac
		self.tictacspin = QSpinBox()
		self.tictacspin.setValue(self.tictac)

		# QSpinBox Sid
		self.sidspin = QSpinBox()
		self.sidspin.setValue(self.sid)

		# QSpinBox Soundfile
		self.soundfilespin = QLineEdit("")
		self.soundfilespin.setText(self.soundfile)

		# Final QLabel
		self.label2 = QLabel()
		self.label2.setAlignment(Qt.AlignCenter)

		# LCD QLabel
		self.lcd = QLabel()
		fontpath = str(dir_path + '/fonts/alarm_clock.ttf')
		ttf = QtGui.QFontDatabase.addApplicationFont(fontpath)
		font = QFont("Alarm Clock", 40, 63)
		self.lcd.setFont(font)
		self.lcd.setStyleSheet("QLabel { background-color : #333; color : white; }");
		self.lcd.setAlignment(Qt.AlignCenter)

		vbox.addWidget(self.startbutton)
		vbox.addWidget(self.stopbutton)
		vbox.addWidget(self.lcd)

		vbox.addWidget(self.get_line())

		# Label Preset
		hbox = QtWidgets.QHBoxLayout()
		vbox.addLayout(hbox)
		hbox.addWidget(QtWidgets.QLabel("Presets"))

		# Add Preset
		hbox = QtWidgets.QHBoxLayout()
		vbox.addLayout(hbox)

		self.setTextPreset = QLineEdit("")
		self.setTextPreset.setMaxLength(30)

		self.btnSave = QPushButton()
		addpath = str(dir_path + '/images/add.png')
		self.btnSave.setIcon(QtGui.QIcon(addpath))
		self.btnSave.setIconSize(QtCore.QSize(24,24))
		self.btnSave.clicked.connect(self.save)

		hbox.addWidget(self.setTextPreset)
		hbox.addWidget(self.btnSave)

		# Select / remove Preset
		hbox = QtWidgets.QHBoxLayout()
		vbox.addLayout(hbox)

		self.btnDelete = QPushButton()
		exitpath = str(dir_path + '/images/exit.png')
		self.btnDelete.setIcon(QtGui.QIcon(exitpath))
		self.btnDelete.setIconSize(QtCore.QSize(24,24))
		self.btnDelete.setMinimumSize(QtCore.QSize(24,24)) 
		self.btnDelete.clicked.connect(self.delete)

		self.presetStore = QStandardItemModel()

		self.updatePresetStore()

		self.comboPreset = QtWidgets.QComboBox(self)
		self.comboPreset.view().setMinimumWidth(180)
		self.comboPreset.setModel(self.presetStore)
		self.comboPreset.currentIndexChanged.connect(self.onPresetComboChanged)
		#self.comboPreset.itemText(self.presetStore)

		hbox.addWidget(self.comboPreset, 6)
		hbox.addWidget(self.btnDelete, 1)

		# Exit Button
		hbox = QtWidgets.QHBoxLayout()
		vbox.addLayout(hbox)

		self.exitbutton = QPushButton("Exit", objectName="Exit")
		self.exitbutton.clicked.connect(self.exit)

		vbox.addWidget(self.exitbutton)

		widget = QWidget()

		# Dark theme Widgets
		if self.dark == 2:
			widget.setStyleSheet('background-color: #555; color: white')
		else:
			widget.setStyleSheet('background-color: #eee; color: black')

		widget.setLayout(vbox)
		self.setCentralWidget(widget)

		self.setWindowTitle('Clocktimer')
		self.setGeometry(300, 300, 250, 150)
		iconpath = str(dir_path + '/images/icon_256x256px.png')
		self.setWindowIcon(QtGui.QIcon(iconpath))

	def setDark(self, dark):
		self.dark = dark
		self.darkspin.setValue(self.dark)

	def setMinutes(self, minutes):
		self.minutes = minutes
		self.editmin.setValue(self.minutes)

	def setSecondes(self, secondes):
		self.secondes = secondes
		self.editsec.setValue(self.secondes)

	def setVolume(self, volume):
		self.volume = volume
		self.player.setVolume(self.volume)

	def setSid(self, sid):
		self.sid = sid
		self.sidspin.setValue(self.sid)

	def setSoundfile(self, soundfile):
		self.soundfile = soundfile
		self.soundfilespin.setText(self.soundfile)

	def setTictac(self, tictac):
		self.tictac = tictac
		self.tictacspin.setValue(self.tictac)

	def singleShot(self, period):
		self.period = period
		p = 60000 - self.period*1000
		self.player.setPosition(p)

	def startTimer(self):
		nummin = self.editmin.text()
		numsec = self.editsec.text()
		self.time = int(nummin) * 60 + int(numsec)
		#print(self.limit)
		self.startbutton.setEnabled(False)

		if self.time > 0:
			self.timer.timeout.connect(self.updateLcd)
			self.timer.start(1000)

		else:
			msginfo = QMessageBox()
			infopath = str(dir_path + '/images/info.png')
			msginfo.setWindowIcon(QtGui.QIcon(infopath))

			# Dark theme info
			if self.dark == 2:
				msginfo.setStyleSheet('background-color: #555; color: white')
			else:
				msginfo.setStyleSheet('background-color: #eee; color: black')

			QMessageBox.information(self, 'Information', "You need to put a positive value")

	def updateLcd(self):
		self.time -= 1
		mins, secs = divmod(self.time, 60)
		timeformat = '{:02d}:{:02d}'.format(mins, secs)
		self.lcd.setText(timeformat)

		if self.time <= 10 and self.tictac != 0:
			#self.tictimer.start()
			#self.tictimer.setInterval(self.time*1000)
			self.player.setMedia(self.soundtictac)
			self.player.play()
			#self.tictimer.timeout.connect(self.abort)

		if self.time == 0:
			self.timer.stop()
			self.player.stop()
			self.player.setMedia(self.sound)
			self.player.play()
			
			# Sound period
			self.limit.start()
			self.limit.setInterval(self.period*1000)
			self.limit.timeout.connect(self.abort)

	def abort(self):
		self.player.stop()
		self.limit.stop()

	def stopTimer(self, period):
		self.player.stop()
		self.timer.stop()
		self.lcd.setText("")
		self.timer = QTimer(self)
		self.time = QTimer(self)
		self.tictimer = QTimer(self)
		self.startbutton.setEnabled(True)

	def save(self):
		name = self.setTextPreset.text()
		minutes = self.editmin.value()
		seconds = self.editsec.value()
		self.app.savePreset(name, minutes, seconds)
		self.updatePresetStore()

	def delete(self):
		index = self.comboPreset.currentIndex()
		if index >= 0:
			ident = self.presetStore.item(index, 0).data(Qt.UserRole + 1)
			#print("Id : %s" % ident)
			self.app.deletePreset(ident)
			self.updatePresetStore()

	def updatePresetStore(self):
		database = self.app.loadDatabase()
		self.presetStore.clear()
		for i in range(len(database)) :
			name = "%s - %d min" % (database[i]['name'], int(database[i]['minutes']))
			if int(database[i]['seconds']) > 0 :
				name += " %d sec" % (int(database[i]['seconds']))

			item = QStandardItem()

			item.setData(name, Qt.DisplayRole)
			item.setData(database[i]['id'], Qt.UserRole + 1)
			item.setData(database[i]['minutes'], Qt.UserRole + 2)
			item.setData(database[i]['seconds'], Qt.UserRole + 3)

			self.presetStore.setItem(i, item)

	def onPresetComboChanged(self, index):
		item = self.presetStore.item(index, 0)
		if item:
			#print("Id : %s" % self.presetStore.item(index, 0).data(Qt.UserRole + 1))
			#print("Minutes : %d" % self.presetStore.item(index, 0).data(Qt.UserRole + 2))
			#print("Secondes : %d" % self.presetStore.item(index, 0).data(Qt.UserRole + 3))
			self.minutes = self.presetStore.item(index, 0).data(Qt.UserRole + 2)
			self.editmin.setValue(self.minutes)
			self.secondes = self.presetStore.item(index, 0).data(Qt.UserRole + 3)
			self.editsec.setValue(self.secondes)

	def about(self):
		am = QMessageBox()
		am.move(470,220)

		aboutpath = str(dir_path + '/images/icon_256x256px.png')
		am.setWindowIcon(QtGui.QIcon(aboutpath))

		# Dark theme about
		if self.dark == 2:
			am.setStyleSheet('background-color: #555; color: white;')
		else:
			am.setStyleSheet('background-color: #eee; color: black')

		QMessageBox.about(am, "About", "Clocktimer Version 1.0<br> GNU General Public License v3.0 only<br><br>Credits :<br><br> Damien Monteillard<br><a style=\"color: #37abc8;\" href=\"https://www.3dminfographie.com\">https://www.3dminfographie.com</a><br><br>Sylvain Philip<br><a style=\"color: #37abc8;\" href=\"http://sphilip.com/\">http://sphilip.com/</a><br><br>Antonio Calculon")

	def pref(self):
		dialog = Preferences(self)

		# Dark theme preferences
		if self.dark == 2:
			dialog.setStyleSheet('background-color: #555; color: white')
		else:
			dialog.setStyleSheet('background-color: #eee; color: black')

		dialog.show()

	def exit(self):
		self.player.stop()
		sys.exit()

	def get_line(self):
		line = QFrame()
		line.setFrameShape(QFrame.HLine)
		line.setStyleSheet('color: #bbb;')
		return line

if __name__ == '__main__':

	app = Application(sys.argv)
	sys.exit(app.exec_())
