#!/usr/bin/env python3

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QDialog, QDial, QSpinBox, QFrame, QCheckBox, QVBoxLayout, QHBoxLayout, QLayout, QLabel
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon, QPixmap
import os

dir_path = os.path.dirname(os.path.realpath(__file__))

class Preferences(QDialog):

	def __init__(self, parent=None):
		super(Preferences, self).__init__(parent)

		# On récupère la fenêtre parente (en l'occurence l'objet Window)
		self.parent = parent

		self.setObjectName("Preferences")
		self.setWindowTitle('Preferences')
		self.resize(360, 125)
		self.move(600,300)

		# Assign a Vbox layout to self
		vbox = QtWidgets.QVBoxLayout(self)

		# Theme dark
		hbox = QtWidgets.QHBoxLayout()
		vbox.addLayout(hbox)
		self.checkdark = QCheckBox("Theme dark")
		self.checkdark.setChecked(parent.dark)
		self.checkdark.toggled.connect(self.checkDark)
		hbox.addWidget(self.checkdark)
		vbox.addWidget(self.get_line())

		# Tic Tac
		hbox = QtWidgets.QHBoxLayout()
		vbox.addLayout(hbox)
		self.checktictac = QCheckBox("Tic Tac")
		self.checktictac.setChecked(parent.tictac)
		self.checktictac.toggled.connect(self.checkState)
		hbox.addWidget(self.checktictac)
		vbox.addWidget(self.get_line())

		# Default minutes
		hbox = QtWidgets.QHBoxLayout()
		vbox.addLayout(hbox)
		hbox.addWidget(QtWidgets.QLabel("Minutes default value"))
		self.spinminutes = QSpinBox()
		self.spinminutes.setMinimum(0)
		self.spinminutes.setMaximum(90)
		self.spinminutes.setValue(parent.minutes)
		hbox.addWidget(self.spinminutes)

		# Default seconds
		hbox = QtWidgets.QHBoxLayout()
		vbox.addLayout(hbox)
		hbox.addWidget(QtWidgets.QLabel("Secondes default value"))
		self.spinsecondes = QSpinBox()
		self.spinsecondes.setMinimum(0)
		self.spinsecondes.setMaximum(59)
		self.spinsecondes.setValue(parent.secondes)
		hbox.addWidget(self.spinsecondes)
		vbox.addWidget(self.get_line())

		# Sound Open File
		hbox = QtWidgets.QHBoxLayout()
		vbox.addLayout(hbox)
		hbox.addWidget(QtWidgets.QLabel("Sound"), 6)

		# Global Sound variable
		self.Sid = int(0)
		self.Sound = str(dir_path + '/sound/ringtimer.ogg')

		self.resetbutton = QtWidgets.QPushButton("Reset")
		self.resetbutton.clicked.connect(self.reset)

		self.openbutton = QtWidgets.QPushButton("Open your file")
		self.openbutton.clicked.connect(self.open)

		hbox.addWidget(self.resetbutton, 1)
		hbox.addWidget(self.openbutton, 3)
		vbox.addWidget(self.get_line())

		# Volume 
		hbox = QtWidgets.QHBoxLayout()
		vbox.addLayout(hbox)
		hbox.addWidget(QtWidgets.QLabel("Volume"))
		self.spin = QSpinBox()
		self.dial = QDial()
		self.dial.setNotchesVisible(True)
		self.dial.setMinimum(0)
		self.dial.setMaximum(99)
		self.dial.setValue(parent.volume)
		self.dial.valueChanged.connect(self.spin.setValue)
		self.spin.setValue(parent.volume)
		self.spin.valueChanged.connect(self.dial.setValue)
		hbox.addWidget(self.dial)
		hbox.addWidget(self.spin)
		vbox.addWidget(self.get_line())

		# Alarm period
		hbox = QtWidgets.QHBoxLayout()
		vbox.addLayout(hbox)
		hbox.addWidget(QtWidgets.QLabel("Alarm period (seconds)"))
		self.spinalarm = QSpinBox()
		self.spinalarm.setMinimum(3)
		self.spinalarm.setMaximum(60)
		self.spinalarm.setValue(parent.period)
		hbox.addWidget(self.spinalarm)

		# Apply Button
		hbox = QtWidgets.QHBoxLayout()
		vbox.addLayout(hbox)
		applybutton = QtWidgets.QPushButton("Save")
		applybutton.clicked.connect(self.return_accept)
		hbox.addWidget(applybutton)

	def get_line(self):
		line = QFrame()
		line.setFrameShape(QFrame.HLine)
		line.setStyleSheet('color: #aaa;')
		return line

	def get_red_led(self):
		self.led_red_path = str(dir_path + '/images/led_red.png')
		self.Led = QtGui.QPixmap(self.led_red_path)
		self.RLed = QLabel()
		self.RLed.setPixmap(self.Led)
		return self.RLed

	def get_green_led(self):
		self.led_green_path = str(dir_path + '/images/led_green.png')
		self.Led = QtGui.QPixmap(self.led_green_path)
		self.GLed = QLabel()
		self.GLed.setPixmap(self.Led)
		return self.GLed

	def reset(self):
		if self.resetbutton.isChecked() == False:
			self.Sid = int(0)
			print(self.Sid)
		else:
			self.Sid = int(1)
			print(self.Sid)

	def open(self):
		self.fileName = QtWidgets.QFileDialog.getOpenFileName(self, "Open File", "~", "Sound (*.ogg *.mp3 *.wav)");
		print(self.fileName[0])
		self.Sound = self.fileName[0]

		if self.openbutton.isChecked() == False:
			self.Sid = int(1)
			print(self.Sid)
		else:
			self.Sid = int(0)
			print(self.Sid)

	def checkState(self):
		if self.checktictac.isChecked() == True:
			tictac = self.checktictac.checkState()
		else:
			tictac = 0

	def checkDark(self):
		if self.checkdark.isChecked() == True:
			dark = self.checkdark.checkState()
		else:
			dark = 0

	# 2 samples functions
	def return_accept(self):
		config = {
			'dark' : self.checkdark.checkState(),
			'tictac' : self.checktictac.checkState(),
			'minutes' : self.spinminutes.value(),
			'secondes' : self.spinsecondes.value(),
			'sid' : self.Sid,
			'soundfile' : self.Sound,
			'volume' : self.dial.value(),
			'period' : self.spinalarm.value(),
		}

		self.parent.setDark(config['dark'])
		self.parent.setTictac(config['tictac'])
		self.parent.setMinutes(config['minutes'])
		self.parent.setSecondes(config['secondes'])
		self.parent.setSid(config['sid'])
		self.parent.setSoundfile(config['soundfile'])
		self.parent.setVolume(config['volume'])
		self.parent.singleShot(config['period'])

		self.parent.app.save_config(config)
		self.close()

	def closeEvent(self, event):
		event.accept()
